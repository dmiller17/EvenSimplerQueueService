// @flow
import Koa from "koa";
import Router from "koa-trie-router";
import BodyParser from "koa-bodyparser";

import EvenSimplerQueueService from "./src/esqs";

const webServer = new Koa();
const router = new Router();
const bodyParser = new BodyParser();

const esqs = new EvenSimplerQueueService(10000);

type Context = {
  body: any,
  request: Object,
  params: Object,
  throw: Function,
};

router
  .get("/status", async (ctx: Context) => {
    ctx.body = esqs.getAllMessagesAndStatuses();
  })
  .get("/messages/:numberOfMessages", async (ctx: Context) => {
    ctx.body = esqs
      .fetchMessages(parseInt(ctx.params.numberOfMessages, 10))
      .toJSON();
  })
  .post("/message", (ctx: Context) => {
    const id = esqs.writeMessage(ctx.request.body);
    ctx.body = id;
  })
  .patch("/message/:id", (ctx: Context) => {
    const id = parseInt(ctx.params.id, 10);

    const processed = esqs.markAsProcessed(id);
    if (processed) {
      ctx.body = "Success";
    } else {
      ctx.body = "Failure";
      ctx.throw(404, `Message ID#${id} not found`);
    }
  });

webServer.use(bodyParser);
webServer.use(router.middleware());

webServer.listen(3000);
