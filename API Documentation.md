# API Documentation

The current API of the EvenSimplerQueueService server is composed of four endpoints.

- `GET /status` \* Returns a representation of the current state of EvenSimplerQueueService. The output consists of two keys: `unprocessed` and `processing`. `unprocessed` messages are those that are available to be consumed by a client, and `processing` messages are those that have been consumed by a client but have not yet been marked as processed.
- `GET /messages/:numberOfMessages` \* Returns a JSON list of messages, where the number of elements in the list of equal to `numberOfMessages`. These messages are moved in to the `processing` state internally, and reflected in the `/status` endpoint.
- `POST /message` \* The client sends a new form encoded message to this endpoint. The request payload can have any number of fields. They will be converted in to the object that will represent the message internally.
- `PATCH /message/:id` \* Marks a message as processed. If a message is not marked as processed within a configurable amount of time the message will be made available to other clients once again.
