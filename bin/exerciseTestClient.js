// @flow
import Client from "../src/testClient.js";

const c = new Client("http://localhost:3000");

const getRandomFail = (): boolean => {
  const min = 0;
  const max = 1;
  const zeroOrOne = Math.floor(Math.random() * 2);

  if (zeroOrOne === 0) {
    return true;
  } else {
    return false;
  }
};

for (let i = 0; i < 100; i++) {
  c.writeMessage({ hello: "world", index: i });
}

for (let i = 0; i < 100; i++) {
  c.fetchMessages(1, false);
}
