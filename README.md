# EvenSimplerQueueService Design Doc

## Abstract

EvenSimplerQueueService (ESQS) is a simple queueing system. Many producers can send messages to ESQS and many consumers can consume messages from ESQS. Consumers can grab a set of messages off of the queue. If the consumer doesn’t confirm that it has processed the messages it pulled, ESQS will make those messages available on the queue again.

## Installation

`yarn && yarn start`

Note that ESQS requires Node.js v7.6.0.

## See it work

To see ESQS in action start it with `yarn start` and then run the testClient with `node bin/exerciseTestClient.js`.

## Implementation

ESQS is a set of simple JavaScript classes wrapped by a web server.

It is backed by an in-memory queue as well as an in-memory hash map. However, nothing about the design prevents it from using any storage engine, persistent or otherwise, that can provide key value semantics.

### High level internal architecture

Past the web server, the ESQS class is a class with three functions. (If I could make the other functions private in JavaScript, I would).

ESQS has two data structures: an internal queue where messages are pushed on to on write, and an internal hash map where messages that have been claimed by a client, but not yet processed, are kept.

The functions work as follows:

1. `writeMessage(message: Object): number`: takes a message, stores it on the queue, assigns it an ID and returns the ID that was assigned.
2. `fetchMessages(numberOfMessages: number = 1): List<Message>` : dequeues the specified number of messages from the queue, places them in a hash map to keep track of which fetched messages haven’t been marked as processed. It fires off an async function that, after a configurable period of time, will move an unprocessed message back in to the queue if it hasn’t yet been processed.
3. `markAsProcessed(messageID: number): bool`: removes the specified message from the hash map of messages still being processed, if present.
4. `getAllMessagesAndStatuses`: returns a list of messages to be processed, and a list of messages that are being processed.

### Technologies Used

To assist in development velocity I used the [Flow](https://flowtype.org) static analysis tool with the type annotations removed at compile time. The core classes are fully annotated with their types.

```
$ yarn flow coverage src/queue.js
Covered: 100.00% (28 of 28 expressions)


$ yarn flow coverage src/esqs.js
Covered: 100.00% (120 of 120 expressions)
```

Unit tests are written in [Jest](https://facebook.github.io/jest/). The core classes have 100% unit test coverage.

```
------------|----------|----------|----------|----------|----------------|
File        |  % Stmts | % Branch |  % Funcs |  % Lines |Uncovered Lines |
------------|----------|----------|----------|----------|----------------|
All files   |      100 |     87.5 |      100 |      100 |                |
 queue.js   |      100 |      100 |      100 |      100 |                |
 esqs.js    |      100 |     87.5 |      100 |      100 |                |
------------|----------|----------|----------|----------|----------------|
```

(Generated with `yarn test — —coverage`)

To expose the core classes over an HTTP interface I used [Koa](http://koajs.com) , a simple web framework, primarily to learn more about `async`/`await` and generators. The web server (found in `index.js`) is simple. Most route functions directly proxy to functions in the core class, although others need to wrangle strings in to proper primitives or perform error handling.

## Trade-offs

A number of trade-offs were made as I implemented ESQS.

### Two internal data structures

I choose to trade space for time by using two data structures. It was easier to keep track of messages being processed in a separate data structure as the access patterns are different.

Unprocessed messages are written and removed, thus a queue felt natural. Messages that are being processed are different: a message needs to be marked as processed by its ID, and it can be accessed at any point. There a hash map felt natural.

It would have been possible to use only one data structure to save space, but it would have been at the expense of the speed of marking a node as processed as you would have to linearly scan the one data structure for the given ID.

### IDs are Integers

(or more accurately JavaScript `number`s)

Each message is given an ID that is an integer. As only once instance of ESQS will ever be run at the same time, this is fine. However, if there were to be multiple instances of ESQS run at the same time then the monotonically increasing IDs would quickly collide, making things confusing for the clients. Given that only one instance is run, I considered this a fine trade off.

### HTTP is the protocol

HTTP is a good first choice as far as protocols go. It’s widely supported, and easy to debug. However it is relatively heavyweight for a message queue, and depending on the environment ESQS is run in it might be better to go with something lighter weight, such as a protocol buffer or thrift RPC system.

## Future Work

If the system needed to scale to more load than could be handled by a single Node.JS process in the future there are a number of changes that I would make.

### Use globally unique message IDs

To scale across multiple processes or servers I would need to change the message ID format. I imagine I could use some combination of timestamp, process ID and/or monotonically increasing number.

### Use a persistent data store

If many processes are going to be handling messages concurrently, we’ll need to share the state of the queue between processes. The easier way to do this would be to use a simple, consistent data store like a MySQL or PostgreSQL.

### Make wider use of `async`

This was the first project I got to use the `async` and `await` keywords in. I think there are opportunities to use them more often, and use them in place of the timeout function that I have in the current version of `removeFromProcessingAfterTimePeriod`.

### Break `messagesBeingProcessed` out in to its own entity

I can already tell that the `ESQS` class is becoming a bit of a god object. It is responsible for many things. A particularly gross violation of the Single Responsibility Principle is that `ESQS` is responsible both for writing messages to an internal queue and managing which messages are still being processed.

With more time I would break out managing which messages are being processed in to its own class.

## Open Issues

- I’m currently using `node-babel` to execute the server. This isn’t particularly efficient as the server needs to transpile the code every time. In the future I would like to pre-transpile the code.
- The status endpoint is _particularly_ barebones. I would love to make a live-updating web page, with React, Redux and Websockets so you could see the state of the system evolve in real time without having to poll an endpoint.
