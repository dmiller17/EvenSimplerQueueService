// @flow
import Queue from "./queue";
import { List } from "immutable";

test("it can be instantiated", () => {
  const queue = new Queue();

  expect(queue).not.toBeUndefined();
});

test("items can be pushed on", () => {
  const queue = new Queue();
  const message = { hello: "world" };

  queue.enqueue(message);

  expect(queue.peek()).toBe(message);
});

test("items can dequeued", () => {
  const queue = new Queue();
  const message = { hello: "world" };

  queue.enqueue(message);

  expect(queue.dequeue()).toBe(message);
  expect(queue.peek()).toBeUndefined();
});

test("queue as list", () => {
  const queue = new Queue();
  const message = { hello: "world" };

  queue.enqueue(message);

  const list = queue.asList();

  expect(list).toEqual(List([message]));
});

test("dequeue empty queue", () => {
  const queue = new Queue();

  expect(queue.dequeue()).toBeUndefined();
});
