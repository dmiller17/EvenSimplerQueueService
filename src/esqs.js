// @flow
import { List, Map } from "immutable";

import Queue from "./queue";

type Message = { id: number, contents: Object };

class EvenSimplerQueueService {
  queue: Queue<Message>;
  maxID: number;
  messagesBeingProcessed: Map<number, Message>;
  moveBackToUnprocessedAfter: number;

  constructor(moveBackToUnprocessedAfter: number = 3000) {
    this.queue = new Queue();
    this.messagesBeingProcessed = new Map();
    this.maxID = 0;
    this.moveBackToUnprocessedAfter = moveBackToUnprocessedAfter;
  }

  // Can't really be more specific about the message that is being
  // submitted, but I will require it to be an object.
  writeMessage(message: Object): number {
    const newID = this.newID();
    this.queue.enqueue({ id: newID, contents: message });

    return newID;
  }

  fetchMessages(numberOfMessages: number = 1): List<Message> {
    let messagesToReturn = new List();

    for (let i = 0; i < numberOfMessages; i++) {
      let message = this.queue.dequeue();
      if (message) {
        messagesToReturn = messagesToReturn.push(message);
        this.messagesBeingProcessed = this.messagesBeingProcessed.set(
          message.id,
          message
        );
        this.removeFromProcessingAfterTimePeriod(message.id);
      }
    }

    return messagesToReturn;
  }

  removeFromProcessingAfterTimePeriod(messageID: number) {
    setTimeout(() => {
      if (this.messagesBeingProcessed.has(messageID)) {
        const message = this.messagesBeingProcessed.get(messageID);
        this.messagesBeingProcessed = this.messagesBeingProcessed.delete(
          messageID
        );

        if (message) {
          this.queue.enqueue(message);
        }
      }
    }, this.moveBackToUnprocessedAfter);
  }

  getAllMessagesAndStatuses(): Map<string, List<Message>> {
    const unprocessed = this.queue.asList();
    const processing = this.messagesBeingProcessed.valueSeq().toList();

    return Map({
      unprocessed: unprocessed,
      processing: processing,
    });
  }

  markAsProcessed(messageID: number): boolean {
    if (this.messagesBeingProcessed.has(messageID)) {
      this.messagesBeingProcessed = this.messagesBeingProcessed.delete(
        messageID
      );
      return true;
    }
    return false;
  }

  newID(): number {
    this.maxID = this.maxID + 1;

    return this.maxID;
  }
}

export default EvenSimplerQueueService;
