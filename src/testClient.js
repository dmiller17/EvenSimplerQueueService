// @flow
import request from "request";

class TestClient {
  url: string;

  constructor(url: string) {
    this.url = url;
  }

  writeMessage(message: Object): void {
    const writeURL = this.url + "/message";

    request.post(
      { url: writeURL, form: message },
      (err, httpResponse, body) => {
        if (!err) {
          const id = parseInt(body, 10);
          console.log(id);
        } else {
          console.error(err);
          throw err;
        }
      }
    );
  }

  fetchMessages(
    numberOfMessagesToFetch: number,
    failToProcess: boolean = false
  ): void {
    const maybeMarkMessageAsProcessed = (
      failToProcess: boolean,
      id: number
    ): void => {
      if (!failToProcess) {
        request.patch(
          this.url + `/message/${id}`,
          (err, httpResponse, body) => {
            if (err) {
              console.error(err);
            }
          }
        );
      }
    };

    let path = `/messages/${numberOfMessagesToFetch}`;

    request(this.url + path, (err, response, body) => {
      if (!err) {
        let messages: Array<Object> = JSON.parse(body);
        messages.forEach((message) => {
          const id = parseInt(message.id, 10);
          maybeMarkMessageAsProcessed(failToProcess, id);
        });
      } else {
        console.error(err);
      }
    });
  }
}

export default TestClient;
