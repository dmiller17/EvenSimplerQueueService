// @flow
import { List, Map } from "immutable";
import EvenSimplerQueueService from "./esqs";

jest.useFakeTimers();

describe("EvenSimplerQueueService", () => {
  it("can be instantiated", () => {
    const wq = new EvenSimplerQueueService();

    expect(wq).not.toBeUndefined();
  });

  it("keeps track of IDs", () => {
    const wq = new EvenSimplerQueueService();

    expect(wq.newID()).toBe(1);
    expect(wq.newID()).toBe(2);
    expect(wq.newID()).toBe(3);
    expect(wq.newID()).toBe(4);
    expect(wq.newID()).toBe(5);
  });

  it("receives a message", () => {
    const wq = new EvenSimplerQueueService();

    const message = { hello: "world" };
    const id = wq.writeMessage(message);

    expect(wq.fetchMessages()).toEqual(List([{ id: 1, contents: message }]));
  });

  it("receives many messages", () => {
    const wq = new EvenSimplerQueueService();
    const testCases = [
      {
        message: { hello: "world" },
        expectedID: 1,
        expectedState: Map({
          unprocessed: List([{ id: 1, contents: { hello: "world" } }]),
          processing: List(),
        }),
      },
      {
        message: { foo: "bar" },
        expectedID: 2,
        expectedState: Map({
          unprocessed: List([
            { id: 2, contents: { foo: "bar" } },
            { id: 1, contents: { hello: "world" } },
          ]),
          processing: List(),
        }),
      },
    ];

    for (let { message, expectedID, expectedState } of testCases) {
      const receivedID = wq.writeMessage(message);

      expect(receivedID).toBe(expectedID);

      const stateOfWQ = wq.getAllMessagesAndStatuses();
      expect(stateOfWQ).toEqual(expectedState);
    }
  });

  it("receives a message and moves it to processing when fetched", () => {
    const wq = new EvenSimplerQueueService();

    const message = { hello: "world" };
    const id = wq.writeMessage(message);

    const messages = wq.fetchMessages();

    expect(messages).toEqual(List([{ id: 1, contents: message }]));

    const stateOfWQ = wq.getAllMessagesAndStatuses();

    expect(stateOfWQ).toEqual(
      Map({
        unprocessed: List(),
        processing: List([{ id: 1, contents: message }]),
      })
    );
  });

  it("fetches specified number of messages", () => {
    const wq = new EvenSimplerQueueService();

    const message = { hello: "world" };
    wq.writeMessage(message);
    wq.writeMessage(message);
    wq.writeMessage(message);
    wq.writeMessage(message);
    wq.writeMessage(message);
    wq.writeMessage(message);

    const messages = wq.fetchMessages(2);

    expect(messages.count()).toBe(2);
  });

  it("can mark a message as processed", () => {
    const wq = new EvenSimplerQueueService();

    const message = { hello: "world" };
    wq.writeMessage(message);
    const messageToMarkAsProcessed = wq.fetchMessages().first();

    expect(
      wq.markAsProcessed(
        messageToMarkAsProcessed ? messageToMarkAsProcessed.id : 0
      )
    ).toBe(true);
  });

  it("cannot mark a message as processed if it has not been fetched", () => {
    const wq = new EvenSimplerQueueService();

    const message = { hello: "world" };
    const id = wq.writeMessage(message);

    expect(wq.markAsProcessed(id)).toBe(false);
  });

  it("shows nodes waiting to be processed and nodes being processed in its status", () => {
    const wq = new EvenSimplerQueueService();

    let firstMessage = { hello: "world" };
    wq.writeMessage(firstMessage);
    const fetchedMessage = wq.fetchMessages().first();
    const id = fetchedMessage ? fetchedMessage.id : 0;
    let secondMessage = { foo: "bar" };
    wq.writeMessage(secondMessage);

    expect(wq.getAllMessagesAndStatuses()).toEqual(
      Map({
        unprocessed: List([{ id: 2, contents: secondMessage }]),
        processing: List([{ id: 1, contents: firstMessage }]),
      })
    );
  });

  it("moves messages back to unproccessed after timeout is reached", () => {
    const wq = new EvenSimplerQueueService();

    const message = { hello: "world" };
    const id = wq.writeMessage(message);

    const fetchedMessages = wq.fetchMessages();
    expect(fetchedMessages).toEqual(List([{ id: 1, contents: message }]));
    expect(wq.getAllMessagesAndStatuses()).toEqual(
      Map({
        unprocessed: List(),
        processing: List([{ id: 1, contents: message }]),
      })
    );

    // but don't mark it as processed, and then let time pass
    jest.runAllTimers();

    expect(wq.getAllMessagesAndStatuses()).toEqual(
      Map({
        unprocessed: List([{ id: 1, contents: message }]),
        processing: List(),
      })
    );
  });
});
