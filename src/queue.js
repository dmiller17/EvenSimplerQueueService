// @flow
import { List } from "immutable";

class Queue<T> {
  list: List<T>;

  constructor() {
    this.list = new List();
  }

  enqueue(message: T): void {
    this.list = this.list.unshift(message);
  }

  dequeue(): ?T {
    const shiftedMessage = this.list.first();

    this.list = this.list.shift();

    return shiftedMessage;
  }

  peek(): ?T {
    return this.list.first();
  }

  asList(): List<T> {
    return this.list;
  }
}

export default Queue;
